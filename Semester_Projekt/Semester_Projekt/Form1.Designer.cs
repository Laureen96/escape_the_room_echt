﻿namespace Semester_Projekt
{
    partial class START
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lab_text = new System.Windows.Forms.Label();
            this.lab_text2 = new System.Windows.Forms.Label();
            this.T_timer_text = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // lab_text
            // 
            this.lab_text.AutoSize = true;
            this.lab_text.Font = new System.Drawing.Font("Yu Gothic", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_text.Location = new System.Drawing.Point(145, 386);
            this.lab_text.Name = "lab_text";
            this.lab_text.Size = new System.Drawing.Size(1436, 60);
            this.lab_text.TabIndex = 0;
            this.lab_text.Text = "Ich habe seit Tagen nichts von meinem Freund Tom gehört . . .";
            // 
            // lab_text2
            // 
            this.lab_text2.AutoSize = true;
            this.lab_text2.Font = new System.Drawing.Font("Yu Gothic", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_text2.Location = new System.Drawing.Point(145, 386);
            this.lab_text2.Name = "lab_text2";
            this.lab_text2.Size = new System.Drawing.Size(1732, 70);
            this.lab_text2.TabIndex = 1;
            this.lab_text2.Text = "Daher habe ich mich dazu entschieden zu ihm nachhause zu schauen . . .";
            this.lab_text2.UseCompatibleTextRendering = true;
            this.lab_text2.Visible = false;
            // 
            // T_timer_text
            // 
            this.T_timer_text.Tick += new System.EventHandler(this.T_timer_text_Tick);
            // 
            // START
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1593, 864);
            this.Controls.Add(this.lab_text2);
            this.Controls.Add(this.lab_text);
            this.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Name = "START";
            this.Text = "frm_start";
            this.Load += new System.EventHandler(this.START_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lab_text;
        private System.Windows.Forms.Label lab_text2;
        private System.Windows.Forms.Timer T_timer_text;
    }
}

