﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Semester_Projekt
{
    public partial class START : Form
    {
        //Variables for text aniamtion
        private string text;
        private string texttwo;
        private int num1;
        private int num2;
        public START()
        {
            InitializeComponent();
        }
        private void START_Load(object sender, EventArgs e)
        {
            //Show form full screen
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;

            text = lab_text.Text;
            lab_text.Text = "";

            texttwo = lab_text2.Text;
            lab_text2.Text = "";

            //start Timer
            T_timer_text.Start();
        }

        private void T_timer_text_Tick(object sender, EventArgs e)
        {
            if (num1 < text.Length)
            {
                lab_text.Text = lab_text.Text + text.ElementAt(num1);
                num1++;
            }
            else
            {
                //Make the next text Visible
                lab_text.Visible = false;
                lab_text2.Visible = true;

            }
            if (lab_text2.Visible == true)
            {

                if (num2 < texttwo.Length)
                {
                    lab_text2.Text = lab_text2.Text + texttwo.ElementAt(num2);
                    num2++;
                }
                else
                { 
                    //stop Timer
                    T_timer_text.Stop();

                    Task.Delay(1000).Wait();

                    //Make Text Invisible
                    lab_text2.Visible = false;

                    //Open Home forms
                    frm_Home home = new frm_Home();
                    home.ShowDialog();

                }
            }

        }

       
    }
}
