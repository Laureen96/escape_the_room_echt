﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semester_Projekt
{
    public partial class frm_Home : Form
    {
        public frm_Home()
        {
            InitializeComponent();
        }

        private void frm_Home_Load(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
        }


        private void pictureBox1_Click_2(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you really want to Exit?", "EXIT", MessageBoxButtons.YesNo);


            if (dr == DialogResult.Yes)
            {
                Application.Exit();
            }
            else if (dr == DialogResult.No)
            {
                frm_Home home = new frm_Home();
                this.Close();
                home.ShowDialog();
            }
            else
            {
                Application.Exit();
            }

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frm_room room = new frm_room();
            room.ShowDialog();
        }
    }
}
